package main

import (
	"flag"
	"fmt"
	"io"
	"math/rand"
	"net"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/bluebreezecf/opentsdb-goclient/client"
	"github.com/bluebreezecf/opentsdb-goclient/config"
)

var (
	httpserveraddr    = flag.String("httpserver", ":12346", "opentsdb server ip")
	serverip          = flag.String("serverip", "127.0.0.1:4242", "opentsdb server ip")
	listeneraddr      = flag.String("listenaddr", "0.0.0.0:12345", "listen addr")
	putDuration       = flag.Duration("putduration", 10*time.Second, "put opentsdb duration")
	startWaitDuration = flag.Duration("startwaitduration", 10*time.Second, "wait time before start httpserver")
)
var mu sync.Mutex
var count int
var tsdbClient client.Client

func add() {
	mu.Lock()
	defer mu.Unlock()
	count++
}
func release() {
	mu.Lock()
	defer mu.Unlock()
	count--
}
func put(value int) error {
	if tsdbClient == nil && openTsDb() == false {
		return nil
	}
	data1 := client.DataPoint{
		Metric:    "connectnum",
		Timestamp: time.Now().Unix(),
		Value:     float64(value),
	}
	value2 := rand.Int() % 100
	data2 := client.DataPoint{
		Metric:    "rand",
		Timestamp: time.Now().Unix(),
		Value:     float64(value2),
	}
	hostname, errHostname := os.Hostname()
	if errHostname != nil {
		return fmt.Errorf("get hostname err:%v", errHostname)
	}
	data1.Tags = map[string]string{"host": hostname}
	data2.Tags = map[string]string{"host": hostname}

	if _, err := tsdbClient.Put([]client.DataPoint{data1, data2}, "details"); err != nil {
		return fmt.Errorf("Error occurs when putting datapoints: %v", err)
	}
	fmt.Printf("put %s value1=%d, value2=%d\n", hostname, value, value2)
	return nil
}
func openTsDb() bool {
	opentsdbCfg := config.OpenTSDBConfig{
		OpentsdbHost: *serverip,
	}
	c, err := client.NewClient(opentsdbCfg)
	if err != nil {
		fmt.Printf("%v\n", err)
		return false
	}

	//0. Ping
	if err = c.Ping(); err != nil {
		fmt.Println(err.Error())
		return false
	}

	tsdbClient = c
	return true
}
func main() {
	flag.Parse()
	fmt.Printf("start sleep\n")
	time.Sleep(*startWaitDuration)
	fmt.Printf("stop sleep\n")
	go func() {
		for {
			put(count)
			time.Sleep(*putDuration)
		}
	}()

	if err := installHttp(); err != nil {
		fmt.Printf("install http server err:%v", err)
		return
	}

	tcpAddr, _ := net.ResolveTCPAddr("tcp", *listeneraddr)
	listener, _ := net.ListenTCP("tcp", tcpAddr)
	for {
		conn, err := listener.AcceptTCP()
		if err != nil {
			continue
		}
		go doTCP(conn)
	}
	fmt.Printf("put:%v\n", put(rand.Int()%100))

}
func getConnectnum(w http.ResponseWriter, req *http.Request) {
	fmt.Printf("http get %d\n", count)
	io.WriteString(w, fmt.Sprintf("{\"code\":200,\"msg\":\"success\",\"err\":\"\", \"data\":\"%d\"}", count))
}
func installHttp() error {
	http.HandleFunc("/connectnum", getConnectnum)
	go http.ListenAndServe(*httpserveraddr, nil)
	return nil
}
func doTCP(conn *net.TCPConn) {
	add()
	defer release()
	defer conn.Close()
	//conn.SetKeepAlive(true)
	//conn.SetDeadline(time.Now().Add(10 * time.Second))

	for {
		_, err := conn.Write([]byte("test"))

		if err != nil {
			break
		}
		time.Sleep(1 * time.Second)
	}
}
