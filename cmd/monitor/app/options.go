package options

import (
	"appautoscale/pkg/monitor/metricplugin"
	"appautoscale/pkg/monitor/metricplugin/opentsdb"
	"appautoscale/pkg/monitor/metricplugin/podhttp"

	"github.com/spf13/pflag"
)

type MonitorConfig struct {
	HttpsAddr           string
	ScaleServerAddr     string
	ScaleServerUsername string
	ScaleServerPassword string
	TokenFilePath       string
	CertFilePath        string
	KeyFilePath         string
	Kubeconfig          string
	ContentType         string
	AddrInterFaceName   string
	SvcName             string
	SvcNamespace        string
	Debug               bool
	MetricPlugins       []metricplugin.MetricPlugin
}

func (mc *MonitorConfig) AddFlags(fs *pflag.FlagSet) {
	fs.StringVar(&mc.HttpsAddr, "httpsaddr", mc.HttpsAddr, "Wait server to connect.")
	fs.StringVar(&mc.ScaleServerAddr, "scaleserveraddr", mc.ScaleServerAddr, "scale server addr.")
	fs.StringVar(&mc.ScaleServerUsername, "scaleserverusername", mc.ScaleServerUsername, "scale server login username.")
	fs.StringVar(&mc.ScaleServerPassword, "scaleserverpassword", mc.ScaleServerPassword, "scale server login password.")
	fs.StringVar(&mc.TokenFilePath, "tokenfilepath", mc.TokenFilePath, "The token for client server.")
	fs.StringVar(&mc.CertFilePath, "certfilepath", mc.CertFilePath, "The https cert for client server.")
	fs.StringVar(&mc.KeyFilePath, "keyfilepath", mc.KeyFilePath, "The https key for client server.")
	fs.StringVar(&mc.Kubeconfig, "kubeconfig", mc.Kubeconfig, "Path to kubeconfig file with authorization and master location information.")
	fs.StringVar(&mc.ContentType, "kube-api-content-type", mc.ContentType, "Content type of requests sent to apiserver.")
	fs.BoolVar(&mc.Debug, "debug", mc.Debug, "Display debug log.")
	fs.StringVar(&mc.AddrInterFaceName, "addrInterFaceName", mc.AddrInterFaceName, "The network interface name.")
	fs.StringVar(&mc.SvcName, "svcName", mc.SvcName, "The monitor svc name.")
	fs.StringVar(&mc.SvcNamespace, "svcNamespace", mc.SvcNamespace, "The monitor svc namespace.")
}

func ProbeVolumePlugins() []metricplugin.MetricPlugin {
	ret := []metricplugin.MetricPlugin{}
	ret = append(ret, opentsdb.ProbeVolumePlugins()...)
	ret = append(ret, podhttp.ProbeVolumePlugins()...)
	return ret
}
func NewMonitorConfigg() *MonitorConfig {
	return &MonitorConfig{
		HttpsAddr:         ":3077",
		AddrInterFaceName: "eth0",
		SvcNamespace:      "kube-system",
		SvcName:           "appautoscale",
		ContentType:       "application/vnd.kubernetes.protobuf",
		Debug:             false,
		MetricPlugins:     ProbeVolumePlugins(),
	}
}

func InitFlags() {
	pflag.Parse()
}
