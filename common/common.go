package common

import (
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"

	"k8s.io/kubernetes/pkg/api/v1"
)

const (
	DeploymentIndexLabelName            string  = "appautoscale_deploy_index"
	ReplicaSetIndexLabelName            string  = "appautoscale_replicaset_index"
	ReplicationControllerIndexLabelName string  = "appautoscale_replicationcontroller_index"
	SvcScoreAnn                         string  = "io.enndata.appautoscale/svc_metricsort_score"
	floatMinDiff                        float64 = 0.00000000000001
)

func IsFloatEqual(a, b float64) bool {
	return a+floatMinDiff > b && a-floatMinDiff < b
}

func GetScaleMoniterNamespace(sm *ScaleMoniter) string {
	switch sm.ScaleType {
	case DeploymentReplica:
		ns, _ := GetNamespaceAndNameByKey(*sm.ScaleTemplate.DeploymentReplica)
		return ns
	case ReplicaSetReplica:
		ns, _ := GetNamespaceAndNameByKey(*sm.ScaleTemplate.ReplicaSetReplica)
		return ns
	case ReplicationControllerReplica:
		ns, _ := GetNamespaceAndNameByKey(*sm.ScaleTemplate.ReplicationControllerReplica)
		return ns
	case DeploymentSet:
		return sm.ScaleTemplate.DeploymentSet.Namespace
	case ReplicaSetSet:
		return sm.ScaleTemplate.ReplicaSetSet.Namespace
	case ReplicationControllerSet:
		return sm.ScaleTemplate.ReplicationControllerSet.Namespace
	}
	return ""
}

func GetScaleMoniterTemplateReplica(sm *ScaleMoniter) int {
	switch sm.ScaleType {
	case DeploymentSet:
		return int(*sm.ScaleTemplate.DeploymentSet.Spec.Replicas)
	case ReplicaSetSet:
		return int(*sm.ScaleTemplate.ReplicaSetSet.Spec.Replicas)
	case ReplicationControllerSet:
		return int(*sm.ScaleTemplate.ReplicationControllerSet.Spec.Replicas)
	}
	return 1
}

func GetMoniterMetricKey(sm *ScaleMoniter) string {
	return GetKeyByNamespaceAndName(GetScaleMoniterNamespace(sm), sm.Name)
}

func IsHostPathExist(path string) bool {
	var exist = true
	if _, err := os.Stat(path); os.IsNotExist(err) {
		exist = false
	}
	return exist
}

func GetKeyByNamespaceAndName(namespace, name string) string {
	return fmt.Sprintf("%s/%s", namespace, name)
}

func GetNamespaceAndNameByKey(key string) (namespace, name string) {
	strs := strings.Split(key, "/")
	if len(strs) == 2 {
		return strs[0], strs[1]
	}
	return "default", key
}

func GetIndexFromMap(index string, m map[string]string) int {
	if m == nil {
		return -1
	}
	if str, exist := m[index]; exist == false {
		return -1
	} else {
		i, err := strconv.Atoi(str)
		if err != nil {
			return -1
		}
		return i
	}
}

func FilterEmptyString(strs []string) []string {
	ret := make([]string, 0, len(strs))
	for _, str := range strs {
		if strings.Trim(str, " ") != "" {
			ret = append(ret, str)
		}
	}
	return ret
}

func IsReadyPod(pod *v1.Pod) bool {
	if pod.DeletionTimestamp != nil || pod.Status.Phase != v1.PodRunning {
		return false
	}
	for _, c := range pod.Status.Conditions {
		if c.Type == v1.PodReady {
			return c.Status == v1.ConditionTrue
		}
	}
	return true
}

func GetIPByInterfaceName(name string) (string, error) {
	ifi, err := net.InterfaceByName(name)
	if err != nil {
		return "", err
	}
	addrs, _ := ifi.Addrs()
	for _, addr := range addrs {
		if strings.Contains(addr.String(), ".") {
			s := "/"
			if strings.Contains(addr.String(), ":") {
				s = ":"
			}
			parts := strings.Split(addr.String(), s)
			if len(parts) == 2 {
				return parts[0], nil
			}
		}
	}
	return "", fmt.Errorf("not found ip")
}
