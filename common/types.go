package common

import (
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	v1 "k8s.io/kubernetes/pkg/api/v1"
	extensions "k8s.io/kubernetes/pkg/apis/extensions/v1beta1"
)

type ScaleType string

const (
	DeploymentReplica            ScaleType = "DeploymentReplica"
	ReplicaSetReplica            ScaleType = "ReplicaSetReplica"
	ReplicationControllerReplica ScaleType = "ReplicationControllerReplica"
	DeploymentSet                ScaleType = "DeploymentSet"
	ReplicaSetSet                ScaleType = "ReplicaSetSet"
	ReplicationControllerSet     ScaleType = "ReplicationControllerSet"
)

type ScaleTemplate struct {
	DeploymentReplica            *string                   `json:"deploymentReplica"`
	ReplicaSetReplica            *string                   `json:"replicaSetReplica"`
	ReplicationControllerReplica *string                   `json:"replicationControllerReplica"`
	DeploymentSet                *extensions.Deployment    `json:"deploymentSet"`
	ReplicaSetSet                *extensions.ReplicaSet    `json:"replicaSetSet"`
	ReplicationControllerSet     *v1.ReplicationController `json:"replicationControllerSet"`
}

type SvcPool struct {
	IndexAnnotationKey string                `json:"indexAnnotationKey"`
	Selector           *metav1.LabelSelector `json:"selector"`
	SvcTemplate        *v1.Service           `json:"svcTemplate"`
}

type OpenTsDBMetricGeter struct {
	Url    string            `json:"url"`
	Metric string            `json:"metric"`
	Tags   map[string]string `json:"tags"`
}

type PodHttpMetricGeter struct {
	Path string `json:"path"`
}

type MetricGeter struct {
	OpenTsDBMetricGeter *OpenTsDBMetricGeter `json:"openTsDBMetricGeter"`
	PodHttpMetricGeter  *PodHttpMetricGeter  `json:"podHttpMetricGeter"`
}

type MoniterMetric struct {
	Name           string        `json:"name"`
	UpScaleValue   float64       `json:"upScaleValue"`
	DownScaleValue float64       `json:"downScaleValue"`
	MetricsTime    time.Duration `json:"metricsTime"`
	MetricGeter    MetricGeter   `json:"metricGeter"`
}

type MetricSort struct {
	Weight     int    `json:"weight"`
	MetricName string `json:"metricName"`
}

type ScaleMoniter struct {
	Name             string          `json:"name"`
	CheckInterval    time.Duration   `json:"checkInterval"`
	MaxReplics       int             `json:"maxReplics"`
	MinReplics       int             `json:"minReplics"`
	MoniterMetrics   []MoniterMetric `json:"moniterMetrics"`
	ScaleUpTrigger   string          `json:"scaleUpTrigger"`
	ScaleDownTrigger string          `json:"scaleDownTrigger"`
	ScaleType        ScaleType       `json:"scaleType"`
	ScaleTemplate    ScaleTemplate   `json:"scaleTemplate"`
	SvcPool          *SvcPool        `json:"svcPool"`
	SvcMetricSort    []MetricSort    `json:"svcMetricSort"`
	CanDeleteTrigger string          `json:"canDeleteTrigger"`
}
