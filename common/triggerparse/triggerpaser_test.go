package triggerparse

import (
	"fmt"
	"testing"
)

type FakeJudge struct {
	value bool
	name  string
}

func (fj *FakeJudge) Name() string {
	return fj.name
}
func (fj *FakeJudge) IsUp() (bool, error) {
	return fj.value, nil
}
func (fj *FakeJudge) IsDown() (bool, error) {
	return fj.value, nil
}

func changeToDownInterface(triggers []*FakeJudge) []Down {
	ret := make([]Down, 0, len(triggers))
	for _, t := range triggers {
		ret = append(ret, t)
	}
	return ret
}
func changeToUpInterface(triggers []*FakeJudge) []Up {
	ret := make([]Up, 0, len(triggers))
	for _, t := range triggers {
		ret = append(ret, t)
	}
	return ret
}
func TestScaleOrTest(t *testing.T) {
	testCase := []struct {
		triggerStr   string
		triggers     []*FakeJudge
		expect       bool
		isErrTrigger bool
	}{
		{
			triggerStr:   "name1",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr:   "(name1)",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr:   "(name1)",
			triggers:     []*FakeJudge{&FakeJudge{value: false, name: "name1"}},
			expect:       false,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1 | name2",
			triggers:     []*FakeJudge{&FakeJudge{value: false, name: "name1"}, &FakeJudge{value: false, name: "name2"}},
			expect:       false,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1 | name2",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: false, name: "name2"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1 | name2",
			triggers:     []*FakeJudge{&FakeJudge{value: false, name: "name1"}, &FakeJudge{value: true, name: "name2"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1 | name2",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: true, name: "name2"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1 | name2",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: true, name: "name2"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr:   "(name1 | name2)",
			triggers:     []*FakeJudge{&FakeJudge{value: false, name: "name1"}, &FakeJudge{value: false, name: "name2"}},
			expect:       false,
			isErrTrigger: false,
		},
		{
			triggerStr:   "(name1 | name2)",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: false, name: "name2"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr:   "(name1 | name2)",
			triggers:     []*FakeJudge{&FakeJudge{value: false, name: "name1"}, &FakeJudge{value: true, name: "name2"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1 | name2 | name3",
			triggers:     []*FakeJudge{&FakeJudge{value: false, name: "name1"}, &FakeJudge{value: false, name: "name2"}, &FakeJudge{value: false, name: "name3"}},
			expect:       false,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1 | name2 | name3",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: false, name: "name2"}, &FakeJudge{value: false, name: "name3"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1 | name2 | name3 | name4",
			triggers:     []*FakeJudge{&FakeJudge{value: false, name: "name1"}, &FakeJudge{value: false, name: "name2"}, &FakeJudge{value: false, name: "name3"}, &FakeJudge{value: false, name: "name4"}},
			expect:       false,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1 | name2 | name3 | name4",
			triggers:     []*FakeJudge{&FakeJudge{value: false, name: "name1"}, &FakeJudge{value: false, name: "name2"}, &FakeJudge{value: true, name: "name3"}, &FakeJudge{value: false, name: "name4"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1|name2|name3|name4",
			triggers:     []*FakeJudge{&FakeJudge{value: false, name: "name1"}, &FakeJudge{value: false, name: "name2"}, &FakeJudge{value: true, name: "name3"}, &FakeJudge{value: false, name: "name4"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1|name2|name3|name4",
			triggers:     []*FakeJudge{&FakeJudge{value: false, name: "name1"}, &FakeJudge{value: false, name: "name2"}, &FakeJudge{value: false, name: "name3"}, &FakeJudge{value: false, name: "name4"}},
			expect:       false,
			isErrTrigger: false,
		},
	}

	for _, c := range testCase {
		jd, errd := NewDownTriggerJudge(c.triggerStr, changeToDownInterface(c.triggers))
		if errd != nil && c.isErrTrigger == false {
			t.Errorf("unexpect NewDownTriggerJudge err:%v", errd)
		} else if errd == nil && c.isErrTrigger == true {
			t.Errorf("expect NewDownTriggerJudge err but is ok")
		} else if errd == nil {
			ok := jd.Judge()
			if ok != c.expect {
				t.Errorf("expect value is %t but get %t", c.expect, ok)
			}
		}

		ju, erru := NewUpTriggerJudge(c.triggerStr, changeToUpInterface(c.triggers))
		if erru != nil && c.isErrTrigger == false {
			t.Errorf("unexpect NewDownTriggerJudge err:%v", erru)
		} else if erru == nil && c.isErrTrigger == true {
			t.Errorf("expect NewDownTriggerJudge err but is ok")
		} else if erru == nil {
			ok := ju.Judge()
			if ok != c.expect {
				t.Errorf("expect value is %t but get %t", c.expect, ok)
			}
		}
	}
}

func TestScaleAndTest(t *testing.T) {
	testCase := []struct {
		triggerStr   string
		triggers     []*FakeJudge
		expect       bool
		isErrTrigger bool
	}{
		{
			triggerStr:   "name1 & name2",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: true, name: "name2"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1 & name2",
			triggers:     []*FakeJudge{&FakeJudge{value: false, name: "name1"}, &FakeJudge{value: true, name: "name2"}},
			expect:       false,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1 & name2",
			triggers:     []*FakeJudge{&FakeJudge{value: false, name: "name1"}, &FakeJudge{value: false, name: "name2"}},
			expect:       false,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1 & name2",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: false, name: "name2"}},
			expect:       false,
			isErrTrigger: false,
		},
		{
			triggerStr:   "(name1 & name2)",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: true, name: "name2"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr:   "(name1 & name2)",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: false, name: "name2"}},
			expect:       false,
			isErrTrigger: false,
		},
		{
			triggerStr:   "(name1 & name2 & name3)",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: true, name: "name2"}, &FakeJudge{value: true, name: "name3"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr:   "(name1 & name2 & name3)",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: false, name: "name2"}, &FakeJudge{value: true, name: "name3"}},
			expect:       false,
			isErrTrigger: false,
		},
		{
			triggerStr:   "(name1&name2&name3)",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: false, name: "name2"}, &FakeJudge{value: true, name: "name3"}},
			expect:       false,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1&name2&name3",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: false, name: "name2"}, &FakeJudge{value: true, name: "name3"}},
			expect:       false,
			isErrTrigger: false,
		},
	}

	for _, c := range testCase {
		jd, errd := NewDownTriggerJudge(c.triggerStr, changeToDownInterface(c.triggers))
		if errd != nil && c.isErrTrigger == false {
			t.Errorf("unexpect NewDownTriggerJudge err:%v", errd)
		} else if errd == nil && c.isErrTrigger == true {
			t.Errorf("expect NewDownTriggerJudge err but is ok")
		} else if errd == nil {
			ok := jd.Judge()
			if ok != c.expect {
				t.Errorf("expect value is %t but get %t", c.expect, ok)
			}
		}

		ju, erru := NewUpTriggerJudge(c.triggerStr, changeToUpInterface(c.triggers))
		if erru != nil && c.isErrTrigger == false {
			t.Errorf("unexpect NewDownTriggerJudge err:%v", erru)
		} else if erru == nil && c.isErrTrigger == true {
			t.Errorf("expect NewDownTriggerJudge err but is ok")
		} else if erru == nil {
			ok := ju.Judge()
			if ok != c.expect {
				t.Errorf("expect value is %t but get %t", c.expect, ok)
			}
		}
	}
}
func TestScaleErrTest(t *testing.T) {
	testCase := []struct {
		triggerStr   string
		triggers     []*FakeJudge
		expect       bool
		isErrTrigger bool
	}{
		{
			triggerStr:   "name1 & name2 &",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: true, name: "name2"}, &FakeJudge{value: true, name: "name3"}},
			expect:       true,
			isErrTrigger: true,
		},
		{
			triggerStr:   "(name1 & name2",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: true, name: "name2"}, &FakeJudge{value: true, name: "name3"}},
			expect:       true,
			isErrTrigger: true,
		},
		{
			triggerStr:   "name1 name2",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: true, name: "name2"}, &FakeJudge{value: true, name: "name3"}},
			expect:       true,
			isErrTrigger: true,
		},
		{
			triggerStr:   "name1 & name2)",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: true, name: "name2"}, &FakeJudge{value: true, name: "name3"}},
			expect:       true,
			isErrTrigger: true,
		},
	}
	for _, c := range testCase {
		jd, errd := NewDownTriggerJudge(c.triggerStr, changeToDownInterface(c.triggers))
		if errd != nil && c.isErrTrigger == false {
			t.Errorf("unexpect NewDownTriggerJudge err:%v", errd)
		} else if errd == nil && c.isErrTrigger == true {
			t.Errorf("expect NewDownTriggerJudge err but is ok")
		} else if errd == nil {
			ok := jd.Judge()
			if ok != c.expect {
				t.Errorf("expect value is %t but get %t", c.expect, ok)
			}
		} else {
			fmt.Printf("NewDownTriggerJudge err:%v\n", errd)
		}

		ju, erru := NewUpTriggerJudge(c.triggerStr, changeToUpInterface(c.triggers))
		if erru != nil && c.isErrTrigger == false {
			t.Errorf("unexpect NewDownTriggerJudge err:%v", erru)
		} else if erru == nil && c.isErrTrigger == true {
			t.Errorf("expect NewDownTriggerJudge err but is ok")
		} else if erru == nil {
			ok := ju.Judge()
			if ok != c.expect {
				t.Errorf("expect value is %t but get %t", c.expect, ok)
			}
		} else {
			fmt.Printf("NewDownTriggerJudge err:%v\n", erru)
		}
	}
}
func TestScaleMixedTest(t *testing.T) {
	testCase := []struct {
		triggerStr   string
		triggers     []*FakeJudge
		expect       bool
		isErrTrigger bool
	}{
		{
			triggerStr:   "name1 & name2 | name3",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: true, name: "name2"}, &FakeJudge{value: true, name: "name3"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1 & name2 | name3",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: false, name: "name2"}, &FakeJudge{value: false, name: "name3"}},
			expect:       false,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1 & name2 | name3",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: true, name: "name2"}, &FakeJudge{value: false, name: "name3"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1 & name2 | name3",
			triggers:     []*FakeJudge{&FakeJudge{value: false, name: "name1"}, &FakeJudge{value: true, name: "name2"}, &FakeJudge{value: true, name: "name3"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr:   "name1 & (name2 | name3)",
			triggers:     []*FakeJudge{&FakeJudge{value: true, name: "name1"}, &FakeJudge{value: false, name: "name2"}, &FakeJudge{value: true, name: "name3"}},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr: "(name1 | name2) & (name3 | name4)",
			triggers: []*FakeJudge{
				&FakeJudge{value: true, name: "name1"},
				&FakeJudge{value: false, name: "name2"},
				&FakeJudge{value: true, name: "name3"},
				&FakeJudge{value: false, name: "name4"},
			},
			expect:       true,
			isErrTrigger: false,
		},
		{
			triggerStr: "(name1 | name2) & (name3 | name4)",
			triggers: []*FakeJudge{
				&FakeJudge{value: false, name: "name1"},
				&FakeJudge{value: false, name: "name2"},
				&FakeJudge{value: true, name: "name3"},
				&FakeJudge{value: false, name: "name4"},
			},
			expect:       false,
			isErrTrigger: false,
		},
		{
			triggerStr: "(name1 | name2) & (name3 | name4)",
			triggers: []*FakeJudge{
				&FakeJudge{value: true, name: "name1"},
				&FakeJudge{value: false, name: "name2"},
				&FakeJudge{value: false, name: "name3"},
				&FakeJudge{value: false, name: "name4"},
			},
			expect:       false,
			isErrTrigger: false,
		},
	}

	for _, c := range testCase {
		jd, errd := NewDownTriggerJudge(c.triggerStr, changeToDownInterface(c.triggers))
		if errd != nil && c.isErrTrigger == false {
			t.Errorf("unexpect NewDownTriggerJudge err:%v", errd)
		} else if errd == nil && c.isErrTrigger == true {
			t.Errorf("expect NewDownTriggerJudge err but is ok")
		} else if errd == nil {
			ok := jd.Judge()
			if ok != c.expect {
				t.Errorf("expect value is %t but get %t", c.expect, ok)
			}
		}

		ju, erru := NewUpTriggerJudge(c.triggerStr, changeToUpInterface(c.triggers))
		if erru != nil && c.isErrTrigger == false {
			t.Errorf("unexpect NewDownTriggerJudge err:%v", erru)
		} else if erru == nil && c.isErrTrigger == true {
			t.Errorf("expect NewDownTriggerJudge err but is ok")
		} else if erru == nil {
			ok := ju.Judge()
			if ok != c.expect {
				t.Errorf("expect value is %t but get %t", c.expect, ok)
			}
		}
	}
}
