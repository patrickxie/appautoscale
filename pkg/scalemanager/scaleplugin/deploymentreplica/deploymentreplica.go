package deploymentreplica

import (
	"appautoscale/common"
	"appautoscale/pkg/monitor/metricplugin"
	"appautoscale/pkg/scalemanager/scaleplugin"
	"fmt"
	"time"

	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/kubernetes/pkg/client/clientset_generated/clientset"
)

const (
	pluginName string = "DeploymentReplica"
)

func ProbeVolumePlugins() []scaleplugin.ScalePlugin {
	return []scaleplugin.ScalePlugin{
		&deploymentReplica{},
	}
}

var _ scaleplugin.ScalePlugin = &deploymentReplica{}
var _ scaleplugin.Scale = &deploymentReplicaScale{}

type deploymentReplica struct {
	client *clientset.Clientset
}

func (dr *deploymentReplica) PluginName() string {
	return pluginName
}

func (dr *deploymentReplica) Init(client *clientset.Clientset, mm *metricplugin.MetricManager) error {
	if client == nil {
		return fmt.Errorf("deploymentReplica int client == nil")
	}
	dr.client = client
	return nil
}

func (dr *deploymentReplica) NewScale(sm *common.ScaleMoniter /*t *common.ScaleTemplate, svcPool *common.SvcPool, maxReplica, minReplica int, canDeleteTrigger string, moniterMetrics []common.MoniterMetric*/) (scaleplugin.Scale, error) {
	if sm.ScaleTemplate.DeploymentReplica == nil {
		return nil, fmt.Errorf("ScaleTemplate.DeploymentReplica is nil")
	}
	ns, name := common.GetNamespaceAndNameByKey(*sm.ScaleTemplate.DeploymentReplica)
	return &deploymentReplicaScale{
		namespace:  ns,
		name:       name,
		client:     dr.client,
		maxReplica: sm.MaxReplics,
		minReplica: sm.MinReplics,
	}, nil
}

type deploymentReplicaScale struct {
	namespace, name        string
	client                 *clientset.Clientset
	maxReplica, minReplica int
}

func (drs *deploymentReplicaScale) Clean() error {
	//return drs.scale(0, nil)
	return nil
}

func (drs *deploymentReplicaScale) ScaleTo(replica int) error {
	if replica > drs.maxReplica || replica < drs.minReplica {
		return fmt.Errorf("cannot scale to %d no in [%d, %d]", replica, drs.minReplica, drs.maxReplica)
	}
	return drs.scale(replica, nil)
}

func (drs *deploymentReplicaScale) WaitScaleTo(replica int, timeout time.Duration) error {
	if replica > drs.maxReplica || replica < drs.minReplica {
		return fmt.Errorf("cannot scale to %d no in [%d, %d]", replica, drs.minReplica, drs.maxReplica)
	}
	return drs.scale(replica, &timeout)
}

func (drs *deploymentReplicaScale) IsScaling() (bool, error) {
	deploy, errGet := drs.client.Extensions().Deployments(drs.namespace).Get(drs.name, metav1.GetOptions{})
	if errGet != nil {
		return false, fmt.Errorf("IsScaling deploy %s/%s err get :%v", drs.namespace, drs.name, errGet)
	}
	replica := deploy.Status.Replicas
	if deploy.Spec.Replicas != nil {
		replica = *deploy.Spec.Replicas
	}
	return deploy.Status.UpdatedReplicas != replica, nil
}

func (drs *deploymentReplicaScale) GetCurReplica() (cur, wanted int, err error) {
	deploy, errGet := drs.client.Extensions().Deployments(drs.namespace).Get(drs.name, metav1.GetOptions{})
	if errGet != nil {
		return -1, -1, fmt.Errorf("GetCurReplica deploy %s/%s err get :%v", drs.namespace, drs.name, errGet)
	}
	replica := deploy.Status.Replicas
	if deploy.Spec.Replicas != nil {
		replica = *deploy.Spec.Replicas
	}
	return int(deploy.Status.UpdatedReplicas), int(replica), nil
}

func (drs *deploymentReplicaScale) scaleSimple(newSize int) error {
	deploy, errGet := drs.client.Extensions().Deployments(drs.namespace).Get(drs.name, metav1.GetOptions{})
	if errGet != nil {
		return errGet
	}
	var size int32 = int32(newSize)
	deploy.Spec.Replicas = &size
	_, err := drs.client.Extensions().Deployments(drs.namespace).Update(deploy)
	if err != nil {
		if errors.IsConflict(err) {
			return fmt.Errorf("update deploy %s/%s conflict err:%v", drs.namespace, drs.name, err)
		}
		return fmt.Errorf("update deploy %s/%s update err:%v", drs.namespace, drs.name, err)
	}
	return nil
}

func (drs *deploymentReplicaScale) scale(newSize int, waitTimeOut *time.Duration) error {
	tryScale := func() (bool, error) {
		err := drs.scaleSimple(newSize)
		if err != nil {
			if errors.IsNotFound(err) {
				return false, fmt.Errorf("deploy %s:%s is not found", drs.namespace, drs.name)
			}
			return false, nil
		}
		return true, nil
	}

	if err := wait.Poll(100*time.Millisecond, 3*time.Second, tryScale); err != nil {
		return err
	}

	if waitTimeOut != nil {
		deployment, err := drs.client.Extensions().Deployments(drs.namespace).Get(drs.name, metav1.GetOptions{})
		if err != nil {
			return err
		}
		desiredGeneration := deployment.Generation
		waitFun := func() (bool, error) {
			deployment, err := drs.client.Extensions().Deployments(drs.namespace).Get(drs.name, metav1.GetOptions{})
			if err != nil {
				return false, err
			}
			replica := deployment.Status.Replicas
			if deployment.Spec.Replicas != nil {
				replica = *deployment.Spec.Replicas
			}

			return deployment.Status.ObservedGeneration >= desiredGeneration &&
				deployment.Status.UpdatedReplicas == replica, nil
		}
		err = wait.Poll(time.Second, *waitTimeOut, waitFun)
		if err == wait.ErrWaitTimeout {
			return fmt.Errorf("timed out waiting for %q to be synced", drs.name)
		}
		return err
	}
	return nil
}
