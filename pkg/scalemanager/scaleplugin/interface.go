package scaleplugin

import (
	"appautoscale/common"
	"appautoscale/log"
	"appautoscale/pkg/monitor/metricplugin"
	"fmt"
	"time"

	"k8s.io/kubernetes/pkg/client/clientset_generated/clientset"
)

type Scale interface {
	Clean() error
	ScaleTo(replica int) error
	WaitScaleTo(replica int, timeout time.Duration) error
	IsScaling() (bool, error)
	GetCurReplica() (cur, wanted int, err error)
}

type ScalePlugin interface {
	PluginName() string
	Init(client *clientset.Clientset, mm *metricplugin.MetricManager) error
	NewScale(sm *common.ScaleMoniter) /* template *common.ScaleTemplate, svcPool *common.SvcPool, maxReplica, minReplica int, canDeleteTrigger string, moniterMetrics []common.MoniterMetric) */ (Scale, error)
}

type ScaleManager struct {
	plugins map[string]ScalePlugin
}

func (sm *ScaleManager) InitPlugin(plugins []ScalePlugin, client *clientset.Clientset, mm *metricplugin.MetricManager) {
	for _, plugin := range plugins {
		name := plugin.PluginName()
		if name == "" {
			continue
		}
		if err := plugin.Init(client, mm); err != nil {
			log.MyLogE("init plugin %s err:%v", name, err)
			continue
		}
		sm.plugins[name] = plugin
	}
}

func (sm *ScaleManager) FindPluginByName(name string) (ScalePlugin, error) {
	plugin, exist := sm.plugins[name]
	if exist == true {
		return plugin, nil
	}
	return nil, fmt.Errorf("plugin %s is not found", name)
}

func NewScaleManager(plugins []ScalePlugin, client *clientset.Clientset, mm *metricplugin.MetricManager) *ScaleManager {
	sm := &ScaleManager{
		plugins: make(map[string]ScalePlugin),
	}
	sm.InitPlugin(plugins, client, mm)
	return sm
}
