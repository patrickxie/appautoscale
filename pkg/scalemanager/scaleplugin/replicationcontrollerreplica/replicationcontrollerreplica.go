package replicationcontrollerreplica

import (
	"appautoscale/common"
	"appautoscale/pkg/monitor/metricplugin"
	"appautoscale/pkg/scalemanager/scaleplugin"
	"fmt"
	"time"

	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/kubernetes/pkg/client/clientset_generated/clientset"
)

const (
	pluginName string = "ReplicationControllerReplica"
)

func ProbeVolumePlugins() []scaleplugin.ScalePlugin {
	return []scaleplugin.ScalePlugin{
		&replicationControllerReplica{},
	}
}

var _ scaleplugin.ScalePlugin = &replicationControllerReplica{}
var _ scaleplugin.Scale = &replicationControllerReplicaScale{}

type replicationControllerReplica struct {
	client *clientset.Clientset
}

func (rcr *replicationControllerReplica) PluginName() string {
	return pluginName
}

func (rcr *replicationControllerReplica) Init(client *clientset.Clientset, mm *metricplugin.MetricManager) error {
	if client == nil {
		return fmt.Errorf("replicationController int client == nil")
	}
	rcr.client = client
	return nil
}

func (rcr *replicationControllerReplica) NewScale(sm *common.ScaleMoniter) (scaleplugin.Scale, error) {
	if sm.ScaleTemplate.ReplicationControllerReplica == nil {
		return nil, fmt.Errorf("ScaleTemplate.ReplicationControllerReplica is nil")
	}
	ns, name := common.GetNamespaceAndNameByKey(*sm.ScaleTemplate.ReplicationControllerReplica)
	return &replicationControllerReplicaScale{
		namespace:  ns,
		name:       name,
		client:     rcr.client,
		maxReplica: sm.MaxReplics,
		minReplica: sm.MinReplics,
	}, nil
}

type replicationControllerReplicaScale struct {
	namespace, name        string
	client                 *clientset.Clientset
	maxReplica, minReplica int
}

func (rcs *replicationControllerReplicaScale) Clean() error {
	//return rcs.scale(0, nil)
	return nil
}

func (rcs *replicationControllerReplicaScale) ScaleTo(replica int) error {
	if replica > rcs.maxReplica || replica < rcs.minReplica {
		return fmt.Errorf("cannot scale to %d no in [%d, %d]", replica, rcs.minReplica, rcs.maxReplica)
	}
	return rcs.scale(replica, nil)
}

func (rcs *replicationControllerReplicaScale) WaitScaleTo(replica int, timeout time.Duration) error {
	if replica > rcs.maxReplica || replica < rcs.minReplica {
		return fmt.Errorf("cannot scale to %d no in [%d, %d]", replica, rcs.minReplica, rcs.maxReplica)
	}
	return rcs.scale(replica, &timeout)
}

func (rcs *replicationControllerReplicaScale) IsScaling() (bool, error) {
	rc, errGet := rcs.client.Core().ReplicationControllers(rcs.namespace).Get(rcs.name, metav1.GetOptions{})
	if errGet != nil {
		return false, fmt.Errorf("IsScaling replicationController %s/%s err get :%v", rcs.namespace, rcs.name, errGet)
	}
	replica := rc.Status.Replicas
	if rc.Spec.Replicas != nil {
		replica = *rc.Spec.Replicas
	}
	return rc.Status.AvailableReplicas != replica, nil
}

func (rcs *replicationControllerReplicaScale) GetCurReplica() (cur, wanted int, err error) {
	rc, errGet := rcs.client.Core().ReplicationControllers(rcs.namespace).Get(rcs.name, metav1.GetOptions{})
	if errGet != nil {
		return -1, -1, fmt.Errorf("GetCurReplica replicationController %s/%s err get :%v", rcs.namespace, rcs.name, errGet)
	}
	replica := rc.Status.Replicas
	if rc.Spec.Replicas != nil {
		replica = *rc.Spec.Replicas
	}
	return int(rc.Status.AvailableReplicas), int(replica), nil
}

func (rcs *replicationControllerReplicaScale) scaleSimple(newSize int) error {
	rc, errGet := rcs.client.Core().ReplicationControllers(rcs.namespace).Get(rcs.name, metav1.GetOptions{})
	if errGet != nil {
		return errGet
	}
	var size int32 = int32(newSize)
	rc.Spec.Replicas = &size
	_, err := rcs.client.Core().ReplicationControllers(rcs.namespace).Update(rc)
	if err != nil {
		if errors.IsConflict(err) {
			return fmt.Errorf("update replicationController %s/%s conflict err:%v", rcs.namespace, rcs.name, err)
		}
		return fmt.Errorf("update replicationController %s/%s update err:%v", rcs.namespace, rcs.name, err)
	}
	return nil
}

func (rcs *replicationControllerReplicaScale) scale(newSize int, waitTimeOut *time.Duration) error {
	tryScale := func() (bool, error) {
		err := rcs.scaleSimple(newSize)
		if err != nil {
			if errors.IsNotFound(err) {
				return false, fmt.Errorf("replicationController %s:%s is not found", rcs.namespace, rcs.name)
			}
			return false, nil
		}
		return true, nil
	}

	if err := wait.Poll(100*time.Millisecond, 3*time.Second, tryScale); err != nil {
		return err
	}

	if waitTimeOut != nil {
		rc, err := rcs.client.Core().ReplicationControllers(rcs.namespace).Get(rcs.name, metav1.GetOptions{})
		if err != nil {
			return err
		}
		desiredGeneration := rc.Generation
		waitFun := func() (bool, error) {
			rs, err := rcs.client.Core().ReplicationControllers(rcs.namespace).Get(rcs.name, metav1.GetOptions{})
			if err != nil {
				return false, err
			}
			replica := rs.Status.Replicas
			if rc.Spec.Replicas != nil {
				replica = *rc.Spec.Replicas
			}

			return rc.Status.ObservedGeneration >= desiredGeneration &&
				rc.Status.AvailableReplicas == replica, nil
		}
		err = wait.Poll(time.Second, *waitTimeOut, waitFun)
		if err == wait.ErrWaitTimeout {
			return fmt.Errorf("timed out waiting for %q to be synced", rcs.name)
		}
		return err
	}
	return nil
}
