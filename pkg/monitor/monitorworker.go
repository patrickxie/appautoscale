package monitor

import (
	"appautoscale/common"
	"appautoscale/errors"
	"appautoscale/log"
	"appautoscale/pkg/monitor/metricplugin"
	"fmt"
	"sort"
	"sync"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/kubernetes/pkg/api/v1"
	"k8s.io/kubernetes/pkg/client/clientset_generated/clientset"
)

const (
	minCheckInterval time.Duration = 10 * time.Second
)

type MonitorScaleStatus int

const (
	ScaleStatusUpOk MonitorScaleStatus = iota
	ScaleStatusDownOk
	ScaleStatusUpErr
	ScaleStatusDownErr
	ScaleStatusDownErrNotMatch
	ScaleStatusOk
	ScaleStatusUnknow
)

type svcMetricSort struct {
	index          int
	metricScoreSum float64
}
type svcMetricSortList []svcMetricSort

func (smsl svcMetricSortList) Len() int {
	return len(smsl)
}

func (smsl svcMetricSortList) Less(i, j int) bool {
	if common.IsFloatEqual(smsl[i].metricScoreSum, smsl[j].metricScoreSum) == true {
		return smsl[i].index > smsl[j].index
	} else {
		return smsl[i].metricScoreSum < smsl[j].metricScoreSum
	}
}

func (smsl svcMetricSortList) Swap(i, j int) {
	smsl[i], smsl[j] = smsl[j], smsl[i]
}

func getMonitorCheckInterval(sm *common.ScaleMoniter) time.Duration {
	if sm.CheckInterval < minCheckInterval {
		return minCheckInterval
	}
	return sm.CheckInterval
}

type MonitorWorkManager struct {
	mm             *metricplugin.MetricManager
	mu             sync.Mutex
	muScaleStatus  sync.Mutex
	scaleStatusMap map[string]MonitorScaleStatus
	monitorMap     map[string]chan struct{}
	isStopped      bool
	client         *clientset.Clientset
	hs             *HttpScale
}

func NewMonitorWorkManager(mm *metricplugin.MetricManager, client *clientset.Clientset, hs *HttpScale) *MonitorWorkManager {
	return &MonitorWorkManager{
		mm:             mm,
		scaleStatusMap: make(map[string]MonitorScaleStatus),
		client:         client,
		monitorMap:     make(map[string]chan struct{}),
		isStopped:      true,
		hs:             hs,
	}
}
func (mwm *MonitorWorkManager) SetScaleMoniterScaleStatus(ns string, sm *common.ScaleMoniter, status MonitorScaleStatus) {
	key := common.GetKeyByNamespaceAndName(ns, sm.Name)
	mwm.muScaleStatus.Lock()
	defer mwm.muScaleStatus.Unlock()
	mwm.scaleStatusMap[key] = status
}

func (mwm *MonitorWorkManager) GetScaleMoniterScaleStatus(ns string, sm *common.ScaleMoniter) MonitorScaleStatus {
	key := common.GetKeyByNamespaceAndName(ns, sm.Name)
	mwm.muScaleStatus.Lock()
	defer mwm.muScaleStatus.Unlock()
	if status, exist := mwm.scaleStatusMap[key]; exist == false {
		return ScaleStatusUnknow
	} else {
		return status
	}
}
func (mwm *MonitorWorkManager) AddMonitor(ns string, sm *common.ScaleMoniter) error {
	mwm.mu.Lock()
	defer mwm.mu.Unlock()
	key := common.GetKeyByNamespaceAndName(ns, sm.Name)
	if _, exist := mwm.monitorMap[key]; exist == true {
		return fmt.Errorf("%s is added", key)
	}
	stop := make(chan struct{}, 0)
	go mwm.work(ns, sm, stop)
	mwm.monitorMap[key] = stop
	return nil
}

func (mwm *MonitorWorkManager) RemoveMonitor(ns, name string) error {
	mwm.mu.Lock()
	defer mwm.mu.Unlock()
	key := common.GetKeyByNamespaceAndName(ns, name)
	if stop, exist := mwm.monitorMap[key]; exist == false {
		return fmt.Errorf("%s is not exist", key)
	} else {
		close(stop)
		delete(mwm.monitorMap, key)
	}
	return nil
}

func (mwm *MonitorWorkManager) getScaleMoniterPodIndexLabel(sm *common.ScaleMoniter) (indexLabelName string, err error) {
	switch sm.ScaleType {
	case common.DeploymentReplica:
		return "", fmt.Errorf("not support")
	case common.ReplicaSetReplica:
		return "", fmt.Errorf("not support")
	case common.ReplicationControllerReplica:
		return "", fmt.Errorf("not support")
	case common.DeploymentSet:
		return common.DeploymentIndexLabelName, nil
	case common.ReplicaSetSet:
		return common.ReplicaSetIndexLabelName, nil
	case common.ReplicationControllerSet:
		return common.ReplicationControllerIndexLabelName, nil
	}
	return "", fmt.Errorf("unknow err")
}
func (mwm *MonitorWorkManager) getScaleMoniterPodSelector(sm *common.ScaleMoniter) (selector labels.Selector, namespace string, err error) {
	switch sm.ScaleType {
	case common.DeploymentReplica:
		if sm.ScaleTemplate.DeploymentReplica == nil {
			return nil, "", fmt.Errorf("DeploymentReplica ScaleMoniter ScaleTemplate.DeploymentReplica == nil")
		}
		ns, name := common.GetNamespaceAndNameByKey(*sm.ScaleTemplate.DeploymentReplica)
		deploy, errGet := mwm.client.Extensions().Deployments(ns).Get(name, metav1.GetOptions{})
		if errGet != nil {
			return nil, ns, fmt.Errorf("get deploy %s err:%v", *sm.ScaleTemplate.DeploymentReplica, errGet)
		}
		selector, err := metav1.LabelSelectorAsSelector(&metav1.LabelSelector{MatchLabels: deploy.Spec.Template.Labels})
		return selector, ns, err
	case common.ReplicaSetReplica:
		if sm.ScaleTemplate.ReplicaSetReplica == nil {
			return nil, "", fmt.Errorf("ReplicaSetReplica ScaleMoniter ScaleTemplate.ReplicaSetReplica == nil")
		}
		ns, name := common.GetNamespaceAndNameByKey(*sm.ScaleTemplate.ReplicaSetReplica)
		rs, errGet := mwm.client.Extensions().ReplicaSets(ns).Get(name, metav1.GetOptions{})
		if errGet != nil {
			return nil, ns, fmt.Errorf("get replicaset %s err:%v", *sm.ScaleTemplate.ReplicaSetReplica, errGet)
		}
		selector, err := metav1.LabelSelectorAsSelector(&metav1.LabelSelector{MatchLabels: rs.Spec.Template.Labels})
		return selector, ns, err
	case common.ReplicationControllerReplica:
		if sm.ScaleTemplate.ReplicationControllerReplica == nil {
			return nil, "", fmt.Errorf("ReplicationControllerReplica ScaleMoniter ScaleTemplate.ReplicationControllerReplica == nil")
		}
		ns, name := common.GetNamespaceAndNameByKey(*sm.ScaleTemplate.ReplicationControllerReplica)
		rc, errGet := mwm.client.Core().ReplicationControllers(ns).Get(name, metav1.GetOptions{})
		if errGet != nil {
			return nil, ns, fmt.Errorf("get replicaset %s err:%v", *sm.ScaleTemplate.ReplicationControllerReplica, errGet)
		}
		selector, err := metav1.LabelSelectorAsSelector(&metav1.LabelSelector{MatchLabels: rc.Spec.Template.Labels})
		return selector, ns, err
	case common.DeploymentSet:
		if sm.ScaleTemplate.DeploymentSet == nil {
			return nil, "", fmt.Errorf("DeploymentReplica ScaleMoniter ScaleTemplate.DeploymentSet == nil")
		}
		selector, err := metav1.LabelSelectorAsSelector(&metav1.LabelSelector{MatchLabels: sm.ScaleTemplate.DeploymentSet.Spec.Template.Labels})
		return selector, sm.ScaleTemplate.DeploymentSet.Namespace, err
	case common.ReplicaSetSet:
		if sm.ScaleTemplate.ReplicaSetSet == nil {
			return nil, "", fmt.Errorf("ReplicaSetSet ScaleMoniter ScaleTemplate.ReplicaSetSet == nil")
		}
		selector, err := metav1.LabelSelectorAsSelector(&metav1.LabelSelector{MatchLabels: sm.ScaleTemplate.ReplicaSetSet.Spec.Template.Labels})
		return selector, sm.ScaleTemplate.ReplicaSetSet.Namespace, err
	case common.ReplicationControllerSet:
		if sm.ScaleTemplate.ReplicationControllerSet == nil {
			return nil, "", fmt.Errorf("ReplicationControllerSet ScaleMoniter ScaleTemplate.ReplicationControllerSet == nil")
		}
		selector, err := metav1.LabelSelectorAsSelector(&metav1.LabelSelector{MatchLabels: sm.ScaleTemplate.ReplicationControllerSet.Spec.Template.Labels})
		return selector, sm.ScaleTemplate.ReplicationControllerSet.Namespace, err
	}
	return nil, "", fmt.Errorf("unknow err")
}

func (mwm *MonitorWorkManager) getServiceMetricSortIndex(sm *common.ScaleMoniter) ([]int, []float64, error) {
	podListMap, err := mwm.getScaleMoniterPodsMapByIndex(sm)
	if err != nil {
		return []int{}, []float64{}, err
	}
	moniterMetricMap := make(map[string]common.MoniterMetric)
	for _, mm := range sm.MoniterMetrics {
		moniterMetricMap[mm.Name] = mm
	}

	smsl := make(svcMetricSortList, 0, len(podListMap))
	for index, pods := range podListMap {
		sms := svcMetricSort{metricScoreSum: 0, index: index}
		ok := true
	forMetrics:
		for _, metric := range sm.SvcMetricSort {
			if mm, exist := moniterMetricMap[metric.MetricName]; exist == false {
				return []int{}, []float64{}, fmt.Errorf("plugin of metric %s is not found", metric.MetricName)
			} else {
				plugin, err := mwm.mm.FindPlugin(&mm.MetricGeter)
				if err != nil {
					return []int{}, []float64{}, fmt.Errorf("plugin of metric %s is not found", metric.MetricName)
				}
				getterList := make([]metricplugin.MetricGeter, 0, len(pods))
				for _, pod := range pods {
					getter, errNew := plugin.NewMetricGeter(&mm, pod)
					if errNew != nil {
						return []int{}, []float64{}, fmt.Errorf("NewMetricGeter for pod %s:%s with metric getter %s err:%v", pod.Namespace, pod.Name, metric.MetricName, errNew)
					}
					getterList = append(getterList, getter)
				}
				mgl := &metricplugin.MetricGeterList{
					MetricName:     mm.Name,
					Getters:        getterList,
					Duration:       mm.MetricsTime,
					UpScaleValue:   mm.UpScaleValue,
					DownScaleValue: mm.DownScaleValue,
				}
				score, errGetScore := mgl.GetMetricScore()
				if errGetScore != nil {
					//return []int{}, []float64{}, fmt.Errorf("plugin of metric %s GetMetricScore  err:%v", metric.MetricName, errGetScore)
					podsName := make([]string, 0, len(pods))
					for _, pod := range pods {
						podsName = append(podsName, fmt.Sprintf("%s/%s", pod.Namespace, pod.Name))
					}
					log.MyLogE("plugin of metric %s GetMetricScore of %v :err:%v", metric.MetricName, podsName, errGetScore)
					ok = false
					break forMetrics
				}
				sms.metricScoreSum += score * float64(metric.Weight)
			}
		}
		if ok == true {
			smsl = append(smsl, sms)
		}
	}
	sort.Sort(sort.Reverse(smsl))
	ret := make([]int, 0, len(smsl))
	scores := make([]float64, 0, len(smsl))
	for _, sms := range smsl {
		ret = append(ret, sms.index)
		scores = append(scores, sms.metricScoreSum)
	}
	return ret, scores, nil
}

func (mwm *MonitorWorkManager) getScaleMoniterPodsMapByIndex(sm *common.ScaleMoniter) (map[int][]*v1.Pod, error) {
	indexLabelName, errLabelGet := mwm.getScaleMoniterPodIndexLabel(sm)
	if errLabelGet != nil {
		return map[int][]*v1.Pod{}, fmt.Errorf("getScaleMoniterPodIndexLabel of %s err:%v", sm.Name, errLabelGet)
	}
	pods, errGetPod := mwm.getScaleMoniterPods(sm)
	if errGetPod != nil {
		return map[int][]*v1.Pod{}, fmt.Errorf("getScaleMoniterPods of %s err:%v", sm.Name, errGetPod)
	}
	ret := make(map[int][]*v1.Pod)
	for _, pod := range pods {
		index := common.GetIndexFromMap(indexLabelName, pod.Labels)
		if index < 0 {
			return map[int][]*v1.Pod{}, fmt.Errorf("cann't get pod %s:%s index by label name:%s", pod.Namespace, pod.Name, indexLabelName)
		}
		if list, exist := ret[index]; exist == false {
			ret[index] = []*v1.Pod{pod}
		} else {
			list = append(list, pod)
		}
	}
	return ret, nil
}

func (mwm *MonitorWorkManager) getScaleMoniterPods(sm *common.ScaleMoniter) ([]*v1.Pod, error) {
	selector, ns, err := mwm.getScaleMoniterPodSelector(sm)
	if err != nil {
		return []*v1.Pod{}, fmt.Errorf("get ScaleMoniter podselector err :%v", err)
	}
	podList, errList := mwm.client.CoreV1().Pods(ns).List(metav1.ListOptions{})
	if errList != nil {
		return []*v1.Pod{}, fmt.Errorf("list pod of ns:%s :%v", ns, errList)
	}
	ret := make([]*v1.Pod, 0, len(podList.Items))
	for i := range podList.Items {
		pod := &podList.Items[i]
		if common.IsReadyPod(pod) == false {
			continue
		}
		if selector.Matches(labels.Set(pod.Labels)) {
			ret = append(ret, pod)
		}
	}
	return ret, nil
}
func (mwm *MonitorWorkManager) work(namespace string, sm *common.ScaleMoniter, stop <-chan struct{}) {
	key := common.GetKeyByNamespaceAndName(namespace, sm.Name)
	log.MyTagLogI(key, "start work for monitor :%s", sm.Name)
	defer log.MyTagLogI(key, "stop work for monitor :%s", sm.Name)

	errInit := mwm.hs.InitScale(namespace, sm)
	if errInit != nil {
		log.MyTagLogE(key, "InitScale %s err:%v", sm.Name, errInit)
	}
breakAll:
	for {
		nextLoopNeedSleep := true
		select {
		case <-stop:
			break breakAll
		default:
		}
		if errInit != nil {
			errInit = mwm.hs.InitScale(namespace, sm)
			if errInit != nil {
				log.MyTagLogE(key, "InitScale %s err:%v", sm.Name, errInit)
			}
		}
		pods, errGet := mwm.getScaleMoniterPods(sm)
		if errGet != nil {
			log.MyTagLogE(key, "sync monitor %s getPods err:%v", sm.Name, errGet)
		} else if cur, want, errRep := mwm.hs.GetCurReplica(namespace, sm); errRep != nil || cur != want {
			if errRep != nil {
				log.MyTagLogE(key, "getreplica err:%v", errRep)
			} else {
				log.MyTagLogI(key, "wait pod running, cur:%d, want:%d", cur, want)
			}
			//} else if len(pods)+1 < want*common.GetScaleMoniterTemplateReplica(sm) {
			//	log.MyTagLogI(key, "wait pod running, cur pod num:%d, want:%d", len(pods), want*common.GetScaleMoniterTemplateReplica(sm))
		} else {
			mgls := make([]*metricplugin.MetricGeterList, 0, len(sm.MoniterMetrics))
		exit:
			for _, metric := range sm.MoniterMetrics {
				plugin, err := mwm.mm.FindPlugin(&metric.MetricGeter)
				if err != nil {
					break
				}
				getterList := make([]metricplugin.MetricGeter, 0, len(pods))
				for _, pod := range pods {
					getter, errNew := plugin.NewMetricGeter(&metric, pod)
					if errNew != nil {
						log.MyTagLogE(key, "sync monitor %s NewMetricGeter for pod %s:%s err:%v", sm.Name, pod.Namespace, pod.Name, errNew)
						break exit
					}
					getterList = append(getterList, getter)
				}
				mgl := &metricplugin.MetricGeterList{
					Key:            key,
					MetricName:     metric.Name,
					Getters:        getterList,
					Duration:       metric.MetricsTime,
					UpScaleValue:   metric.UpScaleValue,
					DownScaleValue: metric.DownScaleValue,
				}
				mgls = append(mgls, mgl)
			}

			if len(mgls) == len(sm.MoniterMetrics) { // no err
				mt := &metricplugin.MetricTrigger{
					Mgls:        mgls,
					Uptrigger:   sm.ScaleUpTrigger,
					Downtrigger: sm.ScaleDownTrigger,
				}

				if ok, err := mt.IsUp(); ok == true && err == nil {
					log.MyTagLogI(key, "+++++++++++need up")
					if len(pods)+1 < want*common.GetScaleMoniterTemplateReplica(sm) {
						log.MyTagLogI(key, "wait pod running, cur pod num:%d, want:%d, give up scale up", len(pods), want*common.GetScaleMoniterTemplateReplica(sm))
					} else {
						errUp := mwm.hs.ScaleUp(namespace, sm)
						if errUp != nil {
							if errors.IsScaleNotInRange(errUp) == false {
								log.MyTagLogE(key, "scale up err:%v", errUp)
								mwm.SetScaleMoniterScaleStatus(namespace, sm, ScaleStatusUpErr)
							}
						} else {
							mwm.SetScaleMoniterScaleStatus(namespace, sm, ScaleStatusUpOk)
							nextLoopNeedSleep = false
						}
					}
				} else if err != nil {
					log.MyTagLogE(key, "+++++++++++up err:%v", err)
				} else if ok, errDown := mt.IsDown(); ok == true && errDown == nil {
					log.MyTagLogI(key, "-----------need down")
					errDown := mwm.hs.ScaleDown(namespace, sm)
					if errDown != nil {
						if errors.IsScaleNotInRange(errDown) == false {
							if errors.IsScaleDeleteMetricNotMatch(errDown) == true {
								mwm.SetScaleMoniterScaleStatus(namespace, sm, ScaleStatusDownErrNotMatch)
								log.MyTagLogI(key, "scale down err:%v", errDown)
							} else {
								mwm.SetScaleMoniterScaleStatus(namespace, sm, ScaleStatusDownErr)
								log.MyTagLogE(key, "scale down err:%v", errDown)
							}
						}
					} else {
						mwm.SetScaleMoniterScaleStatus(namespace, sm, ScaleStatusDownOk)
						nextLoopNeedSleep = false
					}
				} else if errDown != nil {
					log.MyTagLogI(key, "-----------down err:%v", errDown)
				} else {
					log.MyTagLogI(key, "***********is ok")
					mwm.SetScaleMoniterScaleStatus(namespace, sm, ScaleStatusOk)
				}
			}
		}
		if nextLoopNeedSleep == true {
			time.Sleep(getMonitorCheckInterval(sm))
		}
	}
}

func (mwm *MonitorWorkManager) StopAll() {
	mwm.mu.Lock()
	defer mwm.mu.Unlock()
	for _, stop := range mwm.monitorMap {
		close(stop)
	}
	mwm.monitorMap = make(map[string]chan struct{})
}
